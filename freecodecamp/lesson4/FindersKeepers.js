function findElement(arr, func) {
  let num = 0;
  for(var i=0;i<=arr.length-1;i+=1){
		var result = func(arr[i]);
  	if (result){
  	 num = arr[i];
  	 break;
  	}else{
  	 num = undefined;
  	}
  }
  return num;
}


findElement([1, 3, 5, 8, 9, 10], function(num) { return num % 2 === 0; });