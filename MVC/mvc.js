// Представление ( Wiew ) - Визуализация
var constructorWeiw = function(imgUrl,name,size){
	this.imgUrl = imgUrl;
	this.name = name;
	this.size = size;
};

constructorWeiw.prototype.Wiew = function(){
	var row = document.getElementById("information");
	var result = `<div class="img">
									<img src="${this.imgUrl}" alt="" />
								</div>
								<div class="name">
									<span>${this.name}</span>
								</div>
								<div class="size">
									<span>${this.size}</span>
								</div>`;
	row.innerHTML = result;
};
// Модель ( Model) - Действие пользователя
var ControllerPenguin = {
	leng: 1,
	inquiry:function(numberPenguin){
	var url ="https://codepen.io/beautifulcoder/pen/vmOOLr.js";
	fetch(url)
		.then((data)=> data.json())
		.then((data)=>{
			this.leng = data.length-1;
			var Penguin = data[numberPenguin];
			var PingWiew = new constructorWeiw(Penguin.imageUrl,Penguin.name,Penguin.size);
			PingWiew.Wiew()		
		});
	}
};

// Контроллер ( Controller) - Обработка информации


var pinvinModel = {
	numberPenguin: 0,
	click:function(){
		ControllerPenguin.inquiry(0)
	},
	clickNext:function(){
		(ControllerPenguin.leng > this.numberPenguin) ? this.numberPenguin = this.numberPenguin+1 : this.numberPenguin = 0;
		ControllerPenguin.inquiry(this.numberPenguin);
	},
	clickPrev:function(){
	(this.numberPenguin <= 0) ? pinvinModel.numberPenguin = ControllerPenguin.leng : this.numberPenguin -=1;
	ControllerPenguin.inquiry(this.numberPenguin);
	}
};
// Инициализация (Initialize)
(()=>{
	var result = {
		event: function(){
			var next = document.getElementById("next"),
				prev = document.getElementById("prev");
				pinvinModel.click();
				next.onclick = function() {
					 pinvinModel.clickNext();
				}
				prev.onclick = function() {
					 pinvinModel.clickPrev();
				}
			}
		};
	result.event();
})();