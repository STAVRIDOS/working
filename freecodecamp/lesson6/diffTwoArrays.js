function diffArray(arr1, arr2) {
  var newArr = [],
  	maxLengthArr,
  	minLengthArr;
  if(arr1.length>=arr2.length){
  	maxLengthArr = arr1;
  	minLengthArr = arr2;
  }else{
  	maxLengthArr = arr2;
  	minLengthArr = arr1;
  }
  for(var i = 0;i<maxLengthArr.length;i+=1){
  	for(var b = 0;b<maxLengthArr.length;b+=1){
  		if(maxLengthArr[i] === minLengthArr[b]){
  			delete maxLengthArr[i];
  			delete minLengthArr[b];
  		}
  	}
  }
  for(var i = 0;i < maxLengthArr.length;i+=1){
  	if(maxLengthArr[i]){
  		newArr.push(maxLengthArr[i])
  	}
  	if(minLengthArr[i]){
  		newArr.push(minLengthArr[i])
  	}
  }
  return newArr;
}

diffArray([1, 2, 3, 5], [1, 2, 3, 4, 5]);